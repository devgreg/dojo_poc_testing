from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.contrib import messages
from dojo.models import Finding
from .models import *
from .forms import *
from pytz import timezone
from django.shortcuts import render, get_object_or_404
import os
from django.conf import settings
from django.http.response import HttpResponseRedirect, HttpResponse, Http404

localtz = timezone('America/Chicago')

@user_passes_test(lambda u: u.is_staff)
def add_poc_endpoint(request):
    if  request.method == 'POST':
        form = UploadPocForm(request.POST)
        form.save()
    else:
        form = PocEndpointForm()
    return render(request, 'defectDojo-poc-field/add_poc_endpoint.html',
                  {'form': form,
                   })

@user_passes_test(lambda u: u.is_staff)
def add_poc(request, fid):
    find = Finding.objects.get(id=fid)
    if  request.method == 'POST':
        if find.poc_set:
            poc = find.poc_set.first()
            form = UploadPocForm(request.POST, request.FILES, instance=poc )
        else:
            form = UploadPocForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_poc(request.FILES['file'], find)
            if find.poc_set:
                form.save()
            else:
                poc = form.save(commit = False)
                poc.finding = find
                poc.save()
            messages.add_message(request,
                                messages.SUCCESS,
                                'POC succesfully added to finding.',
                                extra_tags = 'alert-success')
            return HttpResponseRedirect(reverse('view_finding', args=(fid,)))                                                                                             
        else:
            messages.add_message(request,
                                 messages.ERROR,
                                 'POC could not be added.',
                                 extra_tags='alert-danger')
    else:
        if find.poc_set:
            poc = find.poc_set.first()
            form = UploadPocForm(instance=poc)
        else:
            form = UploadPocForm()
    return render(request, 'defectDojo-poc-field/add_poc.html',
                  {'find': find,
                   'form': form,
                   })


def handle_uploaded_poc(f, find):
     name, extension = os.path.splitext(f.name)
     with open(settings.MEDIA_ROOT + 'poc/%s%s' % (find.id, extension),
               'wb+') as destination:
         for chunk in f.chunks():
             destination.write(chunk)
     if not(find.poc_set):
         new_poc = Poc(finding=find, path=settings.MEDIA_ROOT + 'poc/%s%s' % (find.id, extension))
         new_poc.save()

#@user_passes_test(lambda u: u.is_staff)
#def view_poc(request, fid):

@user_passes_test(lambda u: u.is_staff)
def run_poc(request, fid):
    poc = Poc.objects.get(finding=Finding.objects.get(id=fid))
    status = os.system(poc.path)
    if status:
            messages.add_message(request,
                                messages.SUCCESS,
                                "POC run, defect is fixed.",
                                extra_tags = 'alert-success')
            return HttpResponseRedirect(reverse('view_finding', args=(poc.finding.id,)))
    else:
            messages.add_message(request,
                                messages.SUCCESS,
                                'POC run, defect is still present.',
                                extra_tags = 'alert-danger')
            return HttpResponseRedirect(reverse('view_finding', args=(poc.finding.id,)))

@user_passes_test(lambda u: u.is_staff)
def poc_endpoints(request):
    return render(request, 'defectDojo-poc-field/poc_endpoints.html',
                 {})

@user_passes_test(lambda u: u.is_staff)
def add_poc_endpoint(request):
    if  request.method == 'POST':
        form = PocEndpointForm(request.POST)
        form.save()
        return HttpResponseRedirect(reverse('poc_endpoints'))
    else:
        form = PocEndpointForm()
        return render(request, 'defectDojo-poc-field/add_poc_endpoint.html',
                      {'form': form,
                       })
