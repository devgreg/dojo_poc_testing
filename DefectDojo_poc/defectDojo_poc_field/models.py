from django.contrib.auth.models import User
from django.db import models

from dojo.models import Finding

class Poc_Endpoint(models.Model):
    user = models.ForeignKey(User, editable=False)
    ip = models.CharField(max_length=500)
    name = models.CharField(max_length=200)

class Poc(models.Model):
    finding = models.ForeignKey(Finding)
    path = models.CharField(max_length=1000, default='none', editable=False, blank=True, null=True)

