from django.conf.urls import patterns, url
from django.contrib import admin
from django.apps import apps

if not apps.ready:
    apps.get_models()

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^poc/add/(?P<fid>\d+)$',
        'defectDojo_poc_field.views.add_poc',
        name= 'add_poc'),
    url(r'^poc/run/(?P<fid>\d+)$',
        'defectDojo_poc_field.views.run_poc',
        name= 'run_poc'),
    url(r'^poc_endpoints$',
        'defectDojo_poc_field.views.poc_endpoints',
        name= 'poc_endpoints'),
    url(r'^add_poc_endpoint$',
        'defectDojo_poc_field.views.add_poc_endpoint',
        name= 'add_poc_endpoint'),


)
