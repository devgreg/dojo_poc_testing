from django import forms
from .models import Poc, Poc_Endpoint

class UploadPocForm(forms.ModelForm):       
     file = forms.FileField(widget=forms.widgets.FileInput(
         attrs={"accept": ".py"}),
         label="Select Python Script")
     class Meta:
        model = Poc
        exclude = ('finding',)

class PocEndpointForm(forms.ModelForm):
    class Meta:
        model = Poc_Endpoint
        fields = "__all__"
